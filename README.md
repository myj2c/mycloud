# mycloud

#### 介绍
我的世界云端加载插件


#### 使用说明
先把MYCloud-1.0.0.jar[下载链接](https://gitee.com/myj2c/mycloud/raw/master/MYCloud-1.0.0.jar)下载到本地，放到plugins目录中，配置好授权码，会根据授权码去云端加载不同的插件

开发插件要实现这个接口，然后把插件上传到服务器，服务器会自动混淆，用户在mycloud配置好授权码，启动会在云端加载的插件，这个插件是混淆过的，用户获取到插件也无法破解
```
package cn.muyang.plugin.verify;

import cn.muyang.cloud.api.PluginAPI;
import cn.muyang.cloud.api.VerifyAPI;
import cn.muyang.cloud.common.utils.VerifyUtils;
import cn.muyang.plugin.Main;
import cn.muyang.plugin.tools.BuilderManager;

public class VerifyCallback implements VerifyAPI {

    @Override
    public void callback(String s) {
        String message = null;
        try {
            message = VerifyUtils.decryptData(s);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String time = message.split("@")[1]; //获取验证时间
        /**
         * 验证当前时间，如果超过10分钟，则验证失败
         * 用来防止用户抓取数据，提交重复数据
         */
        if (System.currentTimeMillis() > Long.parseLong(time) + 1000 * 60 * 10) {
            return;
        }
        //返回数据格式：Plugin:MYDemo&TRUE@1663598453540 
        //插件名称MYDemo是在服务端配置的1663598453540为当前时间
        final String name = Main.INSTANCE.getName();
        if (message.contains(name + "&TRUE")) {
            //验证成功执行插件初始化
            new BuilderManager().init();
            new PluginAPI(Main.INSTANCE).printVerifyMsg();
        }
    }
}

```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
